<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $limit = 50;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('orders')->insert([ //,
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'city' => $faker->city,
                'address' => $faker->streetAddress,
                'email' => $faker->email,
                'phone' => $faker->e164PhoneNumber(),
                'additionalinfo' => $faker->sentence(),
            ]);
        }
    }
}
