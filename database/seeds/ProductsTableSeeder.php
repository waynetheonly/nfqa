<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'iPhone X',
            'price' => '1499',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non arcu imperdiet,
                              pulvinar ipsum a, cursus felis. Aenean tincidunt sit amet felis ut ultrices. Nunc
                              ultricies nunc est, eu tristique augue lacinia vitae. Vestibulum ante ipsum primis in
                              faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse potenti. Mauris
                              sodales eu massa eu blandit. Sed porta leo eget risus rhoncus tincidunt. Cras lacinia
                              odio tellus, at efficitur magna aliquam nec. Morbi in odio eget velit sodales egestas.
                              Vivamus vehicula libero neque, et suscipit tortor vehicula eu. Donec ac turpis vel felis
                              cursus mollis. Sed et felis vestibulum, dignissim orci at, condimentum erat. Aenean eget
                              commodo augue, nec porttitor tortor. Donec est lorem, dignissim quis placerat et,
                              ullamcorper sit amet est. Phasellus posuere accumsan urna, ac hendrerit augue cursus at.',
            'image' => 'https://i-cdn.phonearena.com/images/articles/301589-image/Apple-iPhone-X.jpg',
        ]);
    }
}
