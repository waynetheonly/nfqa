#Projekto paleidimas:
 1. Klonuoti repozitoriją į norimą direktoriją
 2. Per terminalą ar command line nueiti į nuklonuotą projektą
 3. Su komanda `composer install` įrašyti projektui reikalingus paketus
 4. Nukopijuoti faila `.env.example` ir pervadinti jį į `.env`
 5. Su komanda`php artisan key:generate` sugeneruoti aplikacijos kodą
 6. `.env` faile nurodyti prisijungimą prie duomenų bazės:

- `DB_CONNECTION=mysql`
- `DB_HOST=localhost`
- `DB_PORT=3306`
- `DB_DATABASE=[duombazės_pavadinimas]`
- `DB_USERNAME=[duombazės_prisijungimo_vardas]`
- `DB_PASSWORD=[duombazės_slaptažodis]`

##Projekto paruošimas darbui:

 1. Su komanda `php artisan migrate` skurkite reikalingas duombazės lenteles.
 2. Su komanda `php artisan db:seed` papildykite duombazės lenteles reikalinga informaciją
 3. Jeigu norite iš karto sugeneruoti 50 užsakymų rašykite komandą `php artisan db:seed --class=OrdersTableSeeder`

##Prisijungimai:
###Email address: **root@root**
###Password: **root**