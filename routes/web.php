<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/register', function () {
    return redirect(route('home'));
})->name('register');

Route::get('/', 'HomeController@index')->name('home');
Route::resource('order', 'OrderController');
Route::resource('product', 'ProductController');
