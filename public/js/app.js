$(document).ready(function () {
    $('table#orders_table td').on('click', function () {
        var orderHref = $(this).closest('tr').data("href");
        $(location).attr('href', orderHref);
    });
});