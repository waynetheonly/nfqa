@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4">
                @if (session('order_warning'))
                    <div class="alert alert-warning">
                        {{ session('order_warning') }}
                    </div>
                @endif
                <div class="panel {{ $order->status == 1 ? 'panel-success' : 'panel-default' }}">
                    @if($order->status == 1)
                        <div class="panel-heading">Complete</div>
                    @endif
                    <div class="panel-body">
                        <p><b>Name:</b> {{$order->firstname}} {{$order->lastname}}</p>

                        <p><b>Address:</b> {{$order->address}}, {{$order->city}}</p>

                        <p><b>Email:</b> {{$order->email}}</p>

                        <p><b>Phone:</b> {{$order->phone}}</p>

                        <p><b>Name:</b> {{$order->additionalinfo}}</p>

                        @if($order->status == 0)
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-0 col-lg-offset-0">
                                    <form method="POST" action="{{ route('order.update', $order) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <button id="complete_order"
                                                class="btn btn-success btn-block">
                                            Complete
                                        </button>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
