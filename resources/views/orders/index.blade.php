@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(count($orders) > 0)
                    <div class="panel panel-default">
                        <table id="orders_table" class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th class="text-center">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr data-href="{{ route('order.show', [$order->id]) }}">
                                    <td class="text-center">{{ $order->id }}</td>
                                    <td>{{ $order->firstname }} {{ $order->lastname }}</td>
                                    <td>{{ $order->address }}, {{ $order->city }}</td>
                                    <td>{{ $order->email }}</td>
                                    <td>{{ $order->phone }}</td>
                                    <td class="text-center">
                                        <div class="label label-{{ $order->status == 1 ? 'success' : 'default' }}">
                                            {{ $order->status == 1 ? 'Complete' : 'Incomplete' }}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $orders->links() }}
                @else
                    <p class="text-center">No orders yet.</p>
                @endif
            </div>
        </div>
    </div>
@endsection
