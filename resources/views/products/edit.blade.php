@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('product.update', [$product]) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="thumbnail"><img src="{{ $product->image }}"></div>
                            <div class="form-group">
                                <label for="image">Image upload</label>
                                <input type="file" class="" id="image" name="image">
                            </div>
                        </div>
                        <div class="col-sm-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="name">Product name</label>
                                <input type="text" class="form-control" id="name" value="{{ $product->name }}"
                                       name="name">
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" id="price" value="{{ $product->price }}"
                                       name="price">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" rows="8" id="description" style="max-width: 100%"
                                          name="description">{{ $product->description }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary btn-lg pull-right">
                            <i class="glyphicon glyphicon-save"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
