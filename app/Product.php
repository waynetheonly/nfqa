<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public static function uploadImage($image, $title)
    {
        if (!file_exists(public_path('uploads'))) {
            mkdir(public_path('uploads'));
        }

        $image->move(public_path('uploads'), $title);
    }
}
