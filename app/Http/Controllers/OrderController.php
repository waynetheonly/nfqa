<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(array('index', 'show'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = DB::table('orders')->paginate(15);

        return view('orders.index', ['orders' => $orders]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'city' => 'required|min:2',
            'address' => 'required|min:2',
            'phone' => 'required|regex:/^\+?([0-9]{1,4})\)?[-. ]?([0-9]{10})$/',
            'email' => 'required|email'
        ]);

        $order = new Order();
        $order->fill($request->all());
        $order->save();

        return redirect(route('home'))->with('status', 'Order placed successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('orders.show', ['order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Order $order)
    {
        if ($order->status !== 1) {
            $order->status = 1;
            $order->update();
        } else {
            return redirect()->back()->with('order_warning', 'Order is already complete.');
        }

        return redirect()->back();
    }
}
