<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', ['product' => $product->firstOrFail()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required|min:2',
            'price' => 'required|numeric',
            'description' => 'required',
            'image' => 'mimes:jpeg,bmp,png|max:2048'
        ]);

        $imgTitle = time() . '.' . $request->image->getClientOriginalExtension();

        Product::uploadImage($request->image, $imgTitle);

        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->image = url('uploads') . '/' . $imgTitle;
        $product->update();

        return redirect()->back()->with('status', 'Product updated successfully!');
    }
}
